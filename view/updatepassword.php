<?php
include_once('../vendor/autoload.php');
include "../lib/header.php";

use Noman\User\User;
use Noman\Session\Session;
Session::checkSession();
?>
<?php

if (isset($_GET['id'])){
    $userid = (int)$_GET['id'];
    $sesId = Session::get("id");
    if ($userid != $sesId){
        header("Location: Index.php");
    }
}

$user = new User();

if (array_key_exists('updatepassword',$_POST)){
    $updatepass = $user->updatePassword($userid, $_POST);
}
?>

    <section class="py-3">
        <div class="card text-center">
            <div class="card-header d-flex justify-content-between">
                <h5>Update Password</h5>
                <a href="profile.php?id=<?php echo $userid;?>" class="btn btn-primary">Back</a>
            </div>
            <div class="card-body">
                <!-- Default horizontal form -->
                <form style="width: 60% ;margin: 0 auto" action="" method="post">
                    <?php
                    if (isset($updatepass)){
                        echo $updatepass;
                    }
                    ?>
                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="oldpassword" class="col-sm-3 col-form-label">Old Password<i class="fa fa-lock prefix px-2"></i></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Old Password">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="newpassword" class="col-sm-3 col-form-label ">New Password<i class="fa fa-lock prefix px-2"></i></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <input type="submit" name="updatepassword" class="btn btn-primary btn-md" value="Update Password"/>
                        </div>
                    </div>
                    <!-- Grid row -->
                </form>
                <!-- Default horizontal form -->

            </div>
        </div>
    </section>

<?php include "../lib/footer.php";?>