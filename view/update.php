<?php
include_once('../vendor/autoload.php');
include "../lib/header.php";

use Noman\User\User;
use Noman\Session\Session;
Session::checkSession();

if (array_key_exists('id',$_GET)){
    $userid = (int)$_GET['id'];
}

$user = new User();

if (array_key_exists('update',$_POST)){
    $updateuserdata = $user->updateUserData($userid,$_POST);
}

?>
    <section class="py-3">
        <div class="card">
            <div class="card-header">
                <h5 class="text-center">Update Profile</h5>
            </div>
            <div class="card-body">
                <!-- Default horizontal form -->



                <form style="width: 70% ;margin: 0 auto" action="" method="post">
                    <?php
                    if (isset($updateuserdata)){
                        echo $updateuserdata;
                    }
                    ?>

                    <?php

                    $userdata = $user->getUserById($userid);
                    if ($userdata){
                    ?>

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="name" class="col-sm-2 col-form-label">Name: </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" value="<?php echo $userdata->name ;?>">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="User-Name" class="col-sm-2 col-form-label">User Name: </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username" id="User-Name" value="<?php echo $userdata->username ;?>">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="email" class="col-sm-2 col-form-label">Email: </label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" id="email" value="<?php echo $userdata->email ;?>"">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <div class="row">
                        <div class="col-6">
                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Default input -->
                                <label class="col-sm-4 col-form-label">Gender: </label>
                                <div class="col-sm-8">
                                    <input type="radio" name="gender" id="male" value="Male">
                                    <label for="male">Male</label> ||

                                    <input type="radio" name="gender" id="female" value="Female">
                                    <label for="female">Female</label>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </div>
                        <div class="col-6">
                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Default input -->
                                <label for="country" class="col-sm-4 col-form-label">Country: </label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="country" name="country">
                                        <option value="Bangladesh" name="country">Bangladesh</option>
                                        <option value="Pakistan" name="country">Pakistan</option>
                                        <option value="Srilanka" name="country">Srilanka</option>
                                        <option value="India" name="country">India</option>
                                        <option value="Maldiv" name="country">Maldiv</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </div>
                    </div>

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="subject" class="col-sm-2 col-form-label">Subject: </label>
                        <div class="col-sm-10 form-inline">
                            <div class="form-group form-check">
                                <input class="form-check-input" type="checkbox" value="Math" id="math" name="subject[]">
                                <label class="form-check-label" for="math">Mathematics</label> ||

                                <input class="form-check-input" type="checkbox" value="Physics" id="physics" name="subject[]">
                                <label class="form-check-label" for="physics">Physics</label> ||

                                <input class="form-check-input" type="checkbox" value="CSE" id="cse" name="subject[]">
                                <label class="form-check-label" for="cse">CSE</label> ||

                                <input class="form-check-input" type="checkbox" value="EEE" id="eee" name="subject[]">
                                <label class="form-check-label" for="eee">EEE</label> ||

                                <input class="form-check-input" type="checkbox" value="zoology" id="zoology" name="subject[]">
                                <label class="form-check-label" for="zoology">Zoology</label> ||

                                <input class="form-check-input" type="checkbox" value="chemistry" id="chemistry" name="subject[]">
                                <label class="form-check-label" for="chemistry">Chemistry</label> ||

                                <input class="form-check-input" type="checkbox" value="english" id="english" name="subject[]">
                                <label class="form-check-label" for="english">English</label>
                            </div>
                        </div>
                    </div>
                    <!-- Grid row -->


                    <!-- Grid row -->
                    <div class="form-group row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <input type="submit" name="update" class="btn btn-primary btn-md" value="Update">
                            <a class="btn btn-info btn-md" href="profile.php?id=<?php echo $id;?>">Go Profile</a>
                        </div>
                    </div>
                    <!-- Grid row -->
                    <?php }?>
                </form>

                <!-- Default horizontal form -->


            </div>
        </div>
    </section>

<?php include "../lib/footer.php";?>