<?php
include_once('../vendor/autoload.php');
include "../lib/header.php";

use Noman\User\User;
use Noman\Session\Session;
Session::checkSession();

$user = new User();

if (array_key_exists('id',$_GET)){
    $userid = (int)$_GET['id'];
}

?>


    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <h4>User Profile</h4>
            <span>
                <?php
                $sesId = Session::get("id");
                if ($userid == $sesId){
                    ?>
                <a href="image.php" class="btn btn-primary btn-md">Upload Image</a>
                <?php }?>
                <a href="index.php" class="btn btn-primary btn-md">Back</a>
            </span>
        </div>
        <div >
            <div class="my-3">

                <!-- Card Regular -->
                <div class="card card-cascade" style="width: 70%; margin: 0 auto">
                    <!-- Card image -->
                    <?php
                    $userimage = $user->getUserImage("image");
                    if ($userimage){
                    ?>
                    <div class="view overlay d-flex justify-content-center">
                        <img class="card-img-top img-thumbnail" src="<?php print $userimage['image']; ?>" alt="Card image cap" style="height: 250px;width: 200px;border-radius: 50%">
                    </div>
                        <?php }?>
                <?php
                if (isset($updateuser)){
                    echo $updateuser;
                }
                ?>
                <?php

                $userdata = $user->getUserById($userid);
                if ($userdata){

                ?>
                    <!-- Card content -->
                    <div class="card-body text-center">

                        <ul class="list-group">
                            <li class="list-group-item"><strong class="h5">Name : </strong><span><?php echo $userdata->name ;?></span></li>
                            <li class="list-group-item"><strong class="h5">User Name : </strong><span><?php echo $userdata->username ;?></span></li>
                            <li class="list-group-item"><strong class="h5">Email : </strong><span><?php echo $userdata->email ;?></span></li>
                            <li class="list-group-item"><strong class="h5">Gender : </strong><span><?php echo $userdata->gender ;?></span></li>
                            <li class="list-group-item"><strong class="h5">Country : </strong><span><?php echo $userdata->country ;?></span></li>
                            <li class="list-group-item"><strong class="h5">Subject : </strong><span><?php echo $userdata->subject ;?></span></li>
                            <?php
                            $sesId = Session::get("id");
                            if ($userid == $sesId){
                                ?>
                            <li class="list-group-item">
                                <a href="update.php?id=<?php echo $userid;?>" type="button" class="btn btn-primary btn-md">Upadte Profile</a>
                                <a href="updatepassword.php?id=<?php echo $userid;?>" type="button" class="btn btn-info btn-md">Upadte Password</a>
                            </li>
                            <?php }?>
                        </ul>

                    </div>
               <?php }?>
                </div>
                <!-- Card Regular -->


            </div>
        </div>
    </div>

<?php include "../lib/footer.php";?>