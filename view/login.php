<?php
include_once('../vendor/autoload.php');
include "../lib/header.php";

use Noman\User\User;
use Noman\Session\Session;


Session::checkLogin();
$user = new User();

if (array_key_exists('login',$_POST)){
    $userLogin = $user->userLogin($_POST);
}
?>

    <section class="py-3">
        <div class="card text-center">
            <div class="card-header">
                <h5>User Login</h5>
            </div>
            <div class="card-body">
                <!-- Default horizontal form -->
                <form style="width: 60% ;margin: 0 auto" action="" method="post">
                    <?php
                    if (isset($userLogin)){
                        echo $userLogin;
                    }
                    ?>
                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="email" class="col-sm-3 col-form-label">Email<i class="fa fa-envelope prefix px-2"></i></label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <!-- Default input -->
                        <label for="password" class="col-sm-3 col-form-label ">Password<i class="fa fa-lock prefix px-2"></i></label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="form-group row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <input type="submit" name="login" class="btn btn-primary btn-md" value="Log In"/>
                        </div>
                    </div>
                    <!-- Grid row -->
                </form>
                <!-- Default horizontal form -->

            </div>
        </div>
    </section>

<?php include "../lib/footer.php";?>