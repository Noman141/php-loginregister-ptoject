<?php
include_once('../vendor/autoload.php');

include "../lib/header.php";

use Noman\User\User;
use Noman\Session\Session;
Session::checkSession();
$user = new User();

?>

<section class="mt-3">
    <?php
    $loginmsg = Session::get("loginmsg") ;
    if (isset($loginmsg)){
        echo $loginmsg;
    }
    Session::set("loginmsg",NULL);
    ?>
    <div class="card-header d-flex justify-content-between">
        <h4>User List</h4>
        <h4>Welcome! <strong>
                    <?php
                    $name = Session::get("username");
                    if (isset($name)){
                        echo $name;
                    }
                    ?>
            </strong></h4>
    </div>
        <div class="card-body">
            <!--Table-->
            <table class="table table-striped">

                <!--Table head-->
                <thead class="amber lighten-3">
                <tr>
                    <th class="h5">Serial</th>
                    <th class="h5">Name</th>
                    <th class="h5">Username</th>
                    <th class="h5">Email</th>
                    <th class="h5">Action</th>
                </tr>
                </thead>
                <!--Table head-->
                <?php
                $user = new User();
                $userdata = $user->getUserData();
                if ($userdata){
                $i = 0;

                foreach ($userdata as $sdata){
                $i++
                ?>
                <!--Table body-->
                <tbody>
                <tr>
                    <th scope="row"><?php echo $i ;?></th>
                    <td><?php echo $sdata['name'];?></td>
                    <td><?php echo $sdata['username'];?></td>
                    <td><?php echo $sdata['email'];?></td>
                    <td>
                        <a href="profile.php?id=<?php echo $sdata['id'];?>">View Profile</a>
                    </td>
                </tr>
                <?php } }else{?>
                    <tr><td colspan="5"><h2>No User Data Found</h2></td></tr>
                <?php }?>
                </tbody>
                <!--Table body-->

            </table>
            <!--Table-->
        </div>

    </div>
</section>


<?php include "../lib/footer.php";?>
