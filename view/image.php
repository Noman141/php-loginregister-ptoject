<?php
include_once('../vendor/autoload.php');
include "../lib/header.php";

use Noman\User\User;
use Noman\DB\Database;

$user = new User();
$db = new Database();

?>

<section>
    <div class="card">
        <div class="card-header">
            <h5 class="text-center mb-3">Upload Image</h5>
        </div>
        <div class="card-body ">
            <div class="text-center mb-5">
                <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST"){
                        $permited  = array('jpg', 'jpeg', 'png', 'gif');
                        $file_name = $_FILES['image']['name'];
                        $file_size = $_FILES['image']['size'];
                        $file_temp = $_FILES['image']['tmp_name'];

                        $div = explode('.',$file_name);
                        $file_ext = strtolower(end($div));
                        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
                        $uploaded_image = "../uploads/".$unique_image;

                        if (empty($file_name)){
                            echo "<span class='alert alert-danger'>Please Select An Image !</span>";
                        }elseif ($file_size >1000) {
                            echo "<span class='alert alert-danger'>Image Size should be less then 1MB! </span>";
                        }elseif (in_array($file_ext,$permited) === false){
                            echo "<span class='alert alert-danger'>You Can Upload Only :- ".implode(', ',$permited). " files!</span>";
                        }else {
                            move_uploaded_file($file_temp, $uploaded_image);
                            $query = "INSERT INTO user_image (image) VALUES('$uploaded_image')";
                            $inserted_rows = $db->pdo->prepare($query);
//                            $inserted_rows->execute();

                            if ($inserted_rows->execute()) {
                                echo "<span class='alert alert-success'>Image Inserted Successfully. </span>";
    //                                        header("Location: profile.php");
                            } else {
                                echo "<span class='alert alert-danger'>Image Not Inserted !</span>";
                            }
                        }
                    }

                ?>
            </div>

            <form action="" method="post" enctype="multipart/form-data" style="width: 70%;margin: 0 auto">
                <!-- Grid row -->

                <div class="form-group mt-3">

                <div class="row">
                    <!-- Default input -->
                    <label for="image" class="col-sm-4 col-form-label">Upload Image: </label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control-file" id="image" name="image">
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-8">
                        <input type="submit" name="submit" class="btn btn-primary btn-md text-start" value="Submit">
                    </div>
                </div>
             </div>
            </form>
        </div>
    </div>

</section>

<!--<div class="container text-center">-->
<!--    --><?php
//    $permited  = array('jpg', 'jpeg', 'png', 'gif');
//    $sql = "SELECT * FROM user_image ORDER BY id DESC LIMIT 1";
//    $stmt = $db->pdo->prepare($sql);
//    $stmt->execute();
//    $file = $stmt->fetch();
//    header('Content-type: '.'$permited');
//    ?>
<!--     <img src="--><?php //print $file['image']; ?><!--" height="100px " width="200px" alt="Image">-->
<!--</div>-->

<?php include "../lib/footer.php"; ?>