<?php
use Noman\Session\Session;
Session::init();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LogIn Register Project</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="../css/mdb.min.css" rel="stylesheet">
</head>
<?php
if (isset($_GET['action']) && $_GET['action'] == "logout"){
    Session::destroy();
}
?>
<body>

<!-- Start your project here-->
<header>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-light warning-color lighten-5 py-3">

        <div class="container">
            <!-- Navbar brand -->
            <a class="navbar-brand" href="../view/index.php">Noman</a>

            <!-- Collapse button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav" aria-controls="basicExampleNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Collapsible content -->
            <div class="collapse navbar-collapse d-flex justify-content-end" id="basicExampleNav">
                <?php
                $id = Session::get("id");
                $userlogin = Session::get("login");
                if ($userlogin == true){
                ?>
                <!-- Links -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../view/index.php">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../view/profile.php?id=<?php echo $id;?>">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="?action=logout">LogOut</a>
                    </li>
                    <?php }else{ ?>
                    <li class="nav-item d-flex">
                        <a class="nav-link black-text" href="../view/register.php">Register</a>
                    </li>
                    <li class="nav-item d-flex">
                        <a class="nav-link  black-text" href="../view/login.php">LogIn</a>
                    </li>
                    <?php }?>

                </ul>
                <!-- Links -->
            </div>
            <!-- Collapsible content -->
        </div>

    </nav>
    <!--/.Navbar-->
</header>

<div class="container">

