</div>
<footer class="warning-color lighten-5 py-3">
<div class="container">
    <div class="row">
        <div class="col-6">
            <small class="col-6">Project Done By Noman</small>
        </div>

        <div class="col-6 d-flex justify-content-end">
            <small class="col-6">Copy Right @2018</small>
        </div>
    </div>
</div>
</footer>
<!-- /Start your project here-->

<!-- SCRIPTS -->
<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.min.js"></script>
</body>

</html>