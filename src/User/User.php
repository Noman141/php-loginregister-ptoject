<?php
/**
 * Created by PhpStorm.
 * User: Bodhnasha
 * Date: 5/1/2018
 * Time: 1:02 PM
 */

namespace Noman\User;


use Noman\DB\Database;
use Noman\Session\Session;
use PDO;

class User
{
    private $db;

    public function __construct(){
        $this->db = new Database();
    }

    public function userRegistration($data){
        $name = $data['name'];
        $username = $data['username'];
        $email = $data['email'];
        $password = @$data['password'];
        $gender = @$data['gender'];
        $country = $data['country'];
        $subject = @$data['subject'];
        $subject = implode(',', (array)$subject);
        $chk_email = $this->emailCheck($email);


        if ($name == "" OR $username == "" OR $email == "" OR $password == "" OR $gender == "" OR $country == "" OR $subject == ""){
            $msg = "<div class='alert alert-danger'><strong>ERROR!!</strong>Field Shuld Not Be Empty</div>";
            return $msg;
        }

        if (strlen($username) < 3){
            $msg = "<div class='alert alert-danger'><strong>ERROR!!</strong>Username Is Too Short</div>";
            return $msg;
        }

        if (preg_match('/[^a-z0-9_-]+/i',$username)){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Username Must Only Contains Alphanumaric,Underscor and Dashes!!</div>";
            return $msg;
        }

        if (filter_var($email,FILTER_VALIDATE_EMAIL) === false ){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Email Address Is Not Valid</div>";
            return $msg;
        }

        if ($chk_email == true){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Email Is Already Exist!!</div>";
            return $msg;
        }

        if (strlen($password) < 6){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Password Is TOO Short!!</div>";
            return $msg;
        }


        $password = md5($data['password']);

        $sql = "INSERT INTO tbl_user(name, username, email, password, gender, country, subject) VALUES (:name, :username, :email, :password, :gender, :country, :subject) ";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':name',$name);
        $query->bindValue(':username', $username);
        $query->bindValue(':email',$email);
        $query->bindValue(':password',$password);
        $query->bindValue(':gender',$gender);
        $query->bindValue(':country',$country);
        $query->bindValue(':subject',$subject);
        $result = $query->execute();

        if ($result){
            $msg = "<div class='alert alert-success'><strong>Success!</strong>You Are Registerd Now.</div>";
            return $msg;
        }else{
            $msg = "<div class='alert alert-success'><strong>ERROR!</strong>Registration Failed.</div>";
            return $msg;
        }
    }

    public function emailCheck($email){
        $sql = "SELECT email FROM tbl_user WHERE email=:email";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email',$email);
        $query->execute();

        if ($query->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getLogedInUser($email,$password){
        $sql = "SELECT * FROM tbl_user WHERE email =:email AND password =:password LIMIT 1";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email',$email);
        $query->bindValue(':password',$password);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_OBJ);
        return $result;
    }

    public function userLogIn($data){
        $email = $data['email'];
        $password = md5($data['password']);
        $chk_email = $this->emailCheck($email);

        if ($email == "" OR $password == ""){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Field Shuld Not Be Empty!</div>";
            return $msg;
        }

        if (filter_var($email,FILTER_SANITIZE_EMAIL) === false){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Email Is Not Valid!!</div>";
            return $msg;
        }

        if ($chk_email == false){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Email Is Not Exist!!</div>";
            return $msg;
        }

        $result = $this->getLogedInUser($email,$password);

        if ($result){
            Session::init();
            Session::set("login",true);
            Session::set("id",$result->id);
            Session::set("name",$result->name);
            Session::set("username",$result->username);
            Session::set("loginmsg","<div class='alert alert-success'><strong>Success ! </strong>You Are LogedIn!</div>");
            header("Location: index.php");
        }else{
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Data Not Found!!</div>";
            return $msg;
        }
    }

    public function getUserData(){
        $sql = "SELECT * FROM tbl_user ORDER BY id DESC";
        $query = $this->db->pdo->query($sql);
        $query->execute();
        $result = $query->fetchAll();
        return $result;
    }

    public function getUserById($userid){
        $sql = "SELECT * FROM tbl_user WHERE id=:id";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':id',$userid);
        $query->execute();
        $result = $query->fetch (PDO::FETCH_OBJ,true);
        return $result;
    }

    public function updateUserData($id ,$data){
        $name = $data['name'];
        $username = $data['username'];
        $email = $data['email'];
        $gender = @$data['gender'];
        $country = $data['country'];
        $subject = @$data['subject'];
        $subject = implode(',', (array)$subject);
        $chk_email = $this->emailCheck($email);

        if ($name == "" OR $username == "" OR $email == "" OR $gender == "" OR $country == "" OR $subject == ""){
            $msg = "<div class='alert alert-danger'><strong>ERROR!!</strong>Field Shuld Not Be Empty</div>";
            return $msg;
        }

        if (strlen($username) < 3){
            $msg = "<div class='alert alert-danger'><strong>ERROR!!</strong>Username Is Too Short</div>";
            return $msg;
        }

        if (preg_match('/[^a-z0-9_-]+/i',$username)){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Username Must Only Contains Alphanumaric,Underscor and Dashes!!</div>";
            return $msg;
        }

        if (filter_var($email,FILTER_VALIDATE_EMAIL) === false ){
            $msg = "<div class='alert alert-danger'><strong>ERROR ! </strong>Email Address Is Not Valid</div>";
            return $msg;
        }

        $sql = "UPDATE tbl_user SET name =:name, username =:username , email =:email ,gender =:gender, country =:country, subject =:subject WHERE id =:id";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':name',$name);
        $query->bindValue(':username', $username);
        $query->bindValue(':email',$email);
        $query->bindValue(':gender',$gender);
        $query->bindValue(':country',$country);
        $query->bindValue(':subject',$subject);
        $query->bindValue(':id',$id);
        $result = $query->execute();

        if ($result){
            $msg = "<div class='alert alert-success'><strong>Success!</strong>Data Update Successfull.</div>";
            return $msg;
        }else{
            $msg = "<div class='alert alert-success'><strong>ERROR!</strong>Data Update Failed.</div>";
            return $msg;
        }

    }

    private function checkPassword($oldpassword,$id){
        $password = md5($oldpassword);
        $sql = "SELECT password FROM tbl_user WHERE id =:id AND password =:password";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':id',$id);
        $query->bindValue(':password',$password);
        $query->execute();

        if ($query->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }

    public function updatePassword($id, $data){
        $oldpassword = $data['oldpassword'];
        $newpassword = $data['newpassword'];
        $chk_pass = $this->checkPassword($oldpassword, $id);

        if ($oldpassword == "" OR $newpassword == ""){
            $msg = "<div class='alert alert-danger'><strong>ERROR!</strong>Field Shuld Not Be Empty</div>";
            return $msg;
        }

        if ($chk_pass == false){
            $msg = "<div class='alert alert-danger'><strong>ERROR!</strong>Old Password Not Exist</div>";
            return $msg;
        }

        if (strlen($newpassword) > 6){
            $msg = "<div class='alert alert-danger'><strong>ERROR!</strong>Password Is Too Short</div>";
            return $msg;
        }

        $password = md5($newpassword);
        $sql = "UPDATE tbl_user SET password =:password WHERE id =:id";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':password',$password);
        $query->bindValue(':id',$id);
        $result = $query->execute();

        if ($result){
            $msg = "<div class='alert alert-success'><strong>Success!</strong>Password Updated Successfull</div>";
            return $msg;
        }else{
            $msg = "<div class='alert alert-danger'><strong>ERROR!</strong>Password Update  Failed.</div>";
            return $msg;
        }
    }

    public function deleteUser($id){
        $sql = "DELETE FROM tbl_user WHERE id =:id";
        $query = $this->db->pdo->query($sql);
        $query->bindValue(':id',$id);
        $result = $query->execute();
        return $result;
    }

    public function getUserImage($file){
        $permited  = array('jpg', 'jpeg', 'png', 'gif');
        $sql = "SELECT * FROM user_image ORDER BY id DESC LIMIT 1";
        $stmt = $this->db->pdo->prepare($sql);
        $stmt->execute();
        $file = $stmt->fetch();
        header('Content-type: '.'$permited');
        return $file;
    }

}