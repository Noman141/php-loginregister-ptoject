<?php
/**
 * Created by PhpStorm.
 * User: Bodhnasha
 * Date: 5/1/2018
 * Time: 1:02 PM
 */

namespace Noman\DB;
use PDO;

class Database
{
    private $hostdb = 'localhost';
    private $namedb = 'user_db';
    private $userdb = 'root';
    private $passdb = '';
    public $pdo;

    public function __construct()
    {
        if (!isset($this->pdo)){
            try{
                $link = new PDO("mysql:host=".$this->hostdb.";dbname=".$this->namedb,$this->userdb,$this->passdb);
                $link->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
                $this->pdo = $link;
            }catch (PDOException $e){
                die("Failed to connect with database" .$e->getMessage());
            }
        }
    }

}